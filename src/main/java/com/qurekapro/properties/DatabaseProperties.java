package com.qurekapro.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class DatabaseProperties {
	
	@Value("${primary.datasource.driverClassName}")
	private String sql_driver;
	
	@Value("${secondary.datasource.url}")
	private String secondary_sql_db_url;
	
	@Value("${secondary.datasource.username}")
	private String secondary_sql_db_userName;
	
	@Value("${secondary.datasource.password}")
	private String secondary_sql_db_password;


	public String getSql_driver() {
		return sql_driver;
	}

	public void setSql_driver(String sql_driver) {
		this.sql_driver = sql_driver;
	}

	public String getSecondary_sql_db_url() {
		return secondary_sql_db_url;
	}

	public void setSecondary_sql_db_url(String secondary_sql_db_url) {
		this.secondary_sql_db_url = secondary_sql_db_url;
	}

	public String getSecondary_sql_db_userName() {
		return secondary_sql_db_userName;
	}

	public void setSecondary_sql_db_userName(String secondary_sql_db_userName) {
		this.secondary_sql_db_userName = secondary_sql_db_userName;
	}

	public String getSecondary_sql_db_password() {
		return secondary_sql_db_password;
	}

	public void setSecondary_sql_db_password(String secondary_sql_db_password) {
		this.secondary_sql_db_password = secondary_sql_db_password;
	}

}
