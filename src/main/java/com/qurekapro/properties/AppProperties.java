package com.qurekapro.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class AppProperties {

	@Value("${refund.api.url}")
	private String refundApiUrl;

	public String getRefundApiUrl() {
		return refundApiUrl;
	}

	public void setRefundApiUrl(String refundApiUrl) {
		this.refundApiUrl = refundApiUrl;
	}

}
