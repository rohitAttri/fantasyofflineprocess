package com.qurekapro.model;

public class SportsMatchCancelRequest {
	private int request_id;
	private String req_status;
	private int r_success;
	private int r_fail;
	private int r_validation;
	private long match_id;
	public int getRequest_id() {
		return request_id;
	}
	public void setRequest_id(int request_id) {
		this.request_id = request_id;
	}
	public String getReq_status() {
		return req_status;
	}
	public void setReq_status(String req_status) {
		this.req_status = req_status;
	}
	public int getR_success() {
		return r_success;
	}
	public void setR_success(int r_success) {
		this.r_success = r_success;
	}
	public int getR_fail() {
		return r_fail;
	}
	public void setR_fail(int r_fail) {
		this.r_fail = r_fail;
	}
	public int getR_validation() {
		return r_validation;
	}
	public void setR_validation(int r_validation) {
		this.r_validation = r_validation;
	}
	public long getMatch_id() {
		return match_id;
	}
	public void setMatch_id(long match_id) {
		this.match_id = match_id;
	}
}
