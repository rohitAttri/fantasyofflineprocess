package com.qurekapro.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.qurekapro.model.SportsMatchCancelRequest;
import com.qurekapro.util.CommonConstants;
import com.qurekapro.util.QueryConstants;

@Repository
public class SportsMatchesDaoImpl implements SportsMatchesDao {

	private static final Logger LOGS = LoggerFactory.getLogger(SportsMatchesDaoImpl.class);
	
	@Autowired
	@Qualifier("primaryDB")
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	@Qualifier("namedPrimaryDB")
	private NamedParameterJdbcTemplate npJdbcTemplate;
	
	@Override
	public SportsMatchCancelRequest getContestRewardUploadRequests(int requestId) throws Exception {
		return jdbcTemplate.query(
				"SELECT `request_id`, `r_success`, `r_fail`, `r_validation`, `req_status`, `match_id` FROM `sports_match_cancel_request` WHERE request_id=? limit 1",
				new Object[] { requestId }, rs -> {
					if (rs.next()) {
						SportsMatchCancelRequest contestRewardRequest = new SportsMatchCancelRequest();
						contestRewardRequest.setRequest_id(rs.getInt("request_id"));
						contestRewardRequest.setR_success(rs.getInt("r_success"));
						contestRewardRequest.setR_fail(rs.getInt("r_fail"));
						contestRewardRequest.setR_validation(rs.getInt("r_validation"));
						contestRewardRequest.setReq_status(rs.getString("req_status"));
						contestRewardRequest.setMatch_id(rs.getLong("match_id"));
						return contestRewardRequest;
					}
					return null;
				});
	}
	
	@Override
	public boolean deactivateContestAutomation(long mid) {
		try {
			int count = jdbcTemplate.update(QueryConstants.DEACTIVATE_CONTEST_AUTOMATION, CommonConstants.CANCEL_CONTEST_AUTOMATION_STATUS, mid);
			if (count > 0) {
				LOGS.info("deactivating automated contests for mid :: {} and rows affected :: {}",mid,count);
				return true;
			}
			LOGS.info("deactivating automated contests for mid :: {} and rows affected :: {}",mid,count);
		} catch (Exception e) {
			LOGS.error("Exception in SportsMatchesDaoImpl deactivateContestAutomation :: {}", e.getMessage());
			e.printStackTrace();
		}
		return false;
	}
	
	@Override
	public boolean cancelMatch(long mid) {
		try {
			if (jdbcTemplate.update(QueryConstants.CANCEL_MATCH, CommonConstants.MATCH_CANCEL_STATUS, mid) > 0){
				LOGS.info("updating match : {} status to cancelled : {}",mid);
				return true;
			}
		} catch (Exception e) {
			LOGS.error("Exception in SportsMatchesDaoImpl cancelMatch(): {}",e.getMessage());
			e.printStackTrace();
		}
		return false;
	}
	
	@Override
	public boolean checkIfMatchIsCancelledAlready(long mid) {
		try {
			int status = jdbcTemplate.queryForObject(QueryConstants.CHECK_CANCELLED_MATCH, new Object[] {mid}, Integer.class);
			if(CommonConstants.MATCH_CANCEL_STATUS == status){
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public void updateSportsMatchCancelRequestId(int request_id, String req_status) throws Exception {
		MapSqlParameterSource mapSqlParameterSource;
		String sql = "update sports_match_cancel_request set modified_date=now(),req_status=:req_status where request_id=:request_id";

		mapSqlParameterSource = new MapSqlParameterSource();
		mapSqlParameterSource.addValue("request_id", request_id);
		mapSqlParameterSource.addValue("req_status", req_status);

		npJdbcTemplate.update(sql, mapSqlParameterSource);
	}

	@Override
	public void increaseSportsMatchesCancelRequestRecordStatus(int request_id, int status) throws Exception{
		MapSqlParameterSource mapSqlParameterSource;
		String sql = "update sports_match_cancel_request set modified_date=now(), r_success=r_success+:r_success,r_fail=r_fail+:r_fail,r_validation=r_validation+:r_validation where request_id=:request_id";

		mapSqlParameterSource = new MapSqlParameterSource();
		mapSqlParameterSource.addValue("request_id", request_id);
		mapSqlParameterSource.addValue("r_success", 0);
		mapSqlParameterSource.addValue("r_fail", 0);
		mapSqlParameterSource.addValue("r_validation", 0);

		switch (status) {
		case CommonConstants.SUCCESS:
			mapSqlParameterSource.addValue("r_success", 1);
			break;
		case CommonConstants.FAILURE:
			mapSqlParameterSource.addValue("r_fail", 1);
			break;
		case CommonConstants.VALIDATION_FAIL:
			mapSqlParameterSource.addValue("r_validation", 1);
			break;

		}

		npJdbcTemplate.update(sql, mapSqlParameterSource);
	}

	@Override
	public List<Map<String, Object>> getAllActiveContestForMid(long match_id) {
		 List<Map<String, Object>> cidList = new ArrayList<>();
		try {
			LOGS.info("getting all active contests for mid : {}",match_id);
			cidList = jdbcTemplate.queryForList(QueryConstants.GET_ALL_ACTIVE_CONTESTS, match_id, CommonConstants.ACTIVE_CONTESTS_STATUS);
		} catch (Exception e) {
			LOGS.error("Exception in SportsMatchesDaoImpl getAllActiveContestForMid() : {}",e.getMessage());
			e.printStackTrace();
		}
		LOGS.info("cids list size : {}",cidList.size());
		return cidList;
	}
}
