package com.qurekapro.dao;

import java.util.List;
import java.util.Map;

import com.qurekapro.model.SportsMatchCancelRequest;

public interface SportsMatchesDao {	
	
	public SportsMatchCancelRequest getContestRewardUploadRequests(int requestId) throws Exception;
	
	boolean deactivateContestAutomation(long mid);

	boolean checkIfMatchIsCancelledAlready(long mid);

	public void updateSportsMatchCancelRequestId(int request_id, String req_status) throws Exception;

	boolean cancelMatch(long mid);

	public void increaseSportsMatchesCancelRequestRecordStatus(int request_id, int status) throws Exception;

	public List<Map<String, Object>> getAllActiveContestForMid(long match_id);
}
