package com.qurekapro.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication

@EnableAsync
@EnableWebMvc
@ComponentScan("com.qurekapro")

public class FantasyOfflineProcessApplication {

	public static void main(String[] args) {
		SpringApplication.run(FantasyOfflineProcessApplication.class, args);
	}

}
