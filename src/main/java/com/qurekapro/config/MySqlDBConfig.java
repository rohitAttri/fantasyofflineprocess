package com.qurekapro.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.qurekapro.properties.DatabaseProperties;

@Configuration
//@EnableTransactionManagement
public class MySqlDBConfig {

	@Autowired
	DatabaseProperties databaseProperties;

	@Bean("primeDB")
	@ConfigurationProperties("primary.datasource")
	@Primary
	public DataSource primeDB() throws Exception {

		DataSource dataSource = DataSourceBuilder.create().build();
		return dataSource;
	}

	@Bean("primaryDB")
	@Autowired
	public JdbcTemplate primaryDB(@Qualifier(value = "primeDB") DataSource primeDB) {
		return new JdbcTemplate(primeDB);
	}
	
	@Bean("namedPrimaryDB")
	@Autowired
	public NamedParameterJdbcTemplate namedPrimaryDB(@Qualifier(value = "primeDB") DataSource primeDB) {
		return new NamedParameterJdbcTemplate(primeDB);
	}
	
	@Bean("secDB")
	public DataSource secDB() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(databaseProperties.getSql_driver());
		dataSource.setUrl(databaseProperties.getSecondary_sql_db_url());
		dataSource.setUsername(databaseProperties.getSecondary_sql_db_userName());
		dataSource.setPassword(databaseProperties.getSecondary_sql_db_password());
		return dataSource;
	}
	
	@Bean("secondaryDB")
	@Autowired
	public JdbcTemplate secondaryDB(@Qualifier(value = "secDB") DataSource secDB) {
		return new JdbcTemplate(secDB);
	}

	/*@Bean("transactionManager")
	@Autowired
	public PlatformTransactionManager transactionManager(@Qualifier(value = "primeDB") DataSource primeDB) {
		return new DataSourceTransactionManager(primeDB);
	}*/

}
