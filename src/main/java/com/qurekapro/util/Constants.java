package com.qurekapro.util;

public class Constants {
	public static final String ENTRY_FEE_TYPE_CASH_DESC = "Cash";
	public static final String ENTRY_FEE_TYPE_COIN_DESC = "Coin";
	public static final String ENTRY_FEE_TYPE_CASH_COIN_DESC = "Cash Coin";
	
	public static final int ENTRY_FEE_TYPE_CASH = 1;
	public static final int ENTRY_FEE_TYPE_COIN = 2;
	public static final int ENTRY_FEE_TYPE_CASH_COIN = 3;
}
