package com.qurekapro.util;

public interface CommonConstants {

	int SUCCESS = 1;
	int FAILURE = 2;
	int VALIDATION_FAIL = -1;

	String REQUEST_COMPLETED = "Completed";
	String REQUEST_PROCESSING = "Processing";
	String REQUEST_PENDING = "Pending";
	
	String VIPWALLET = "vipwallet"; 
	String DEPOSIT = "deposit"; 
	String REWARD = "reward"; 
	String EARNING = "earning"; 
	String FANTASY = "fantasy";

	
	int CANCEL_CONTEST_AUTOMATION_STATUS = 0;
	int MATCH_CANCEL_STATUS = 6; 
	int ACTIVE_CONTESTS_STATUS = 1;

}
