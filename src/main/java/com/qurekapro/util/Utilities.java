package com.qurekapro.util;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFileAttributes;
import java.nio.file.attribute.PosixFilePermission;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;
import java.util.TimeZone;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.stereotype.Component;

@Component
public class Utilities {
	public static void addPermissionToFiles(String filePath) {
		try {
			Path path = Paths.get(filePath);
			Set<PosixFilePermission> perms = Files.readAttributes(path, PosixFileAttributes.class).permissions();

			perms.add(PosixFilePermission.OWNER_WRITE);
			perms.add(PosixFilePermission.OWNER_READ);
			perms.add(PosixFilePermission.OWNER_EXECUTE);
			perms.add(PosixFilePermission.GROUP_WRITE);
			perms.add(PosixFilePermission.GROUP_READ);
			perms.add(PosixFilePermission.GROUP_EXECUTE);
			perms.add(PosixFilePermission.OTHERS_WRITE);
			perms.add(PosixFilePermission.OTHERS_READ);
			perms.add(PosixFilePermission.OTHERS_EXECUTE);
			Files.setPosixFilePermissions(path, perms);

			// System.out.format("Permissions after: %s%n",
			// PosixFilePermissions.toString(perms));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static Date atStartOfDay(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}

	public static Date atEndOfDay(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 999);
		return calendar.getTime();
	}

	public static String subtractFiveAndHalfHrs(Date date) {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.HOUR_OF_DAY, -5);
		calendar.add(Calendar.MINUTE, -30);
		return sdf.format(calendar.getTime());
	}

	public static long timeDifferenceInMinutes(long startTimeInMilliSec, long endTimeInMilliSec) {
		long diffMinutes = 0;
		try {
			long diffMillSec = endTimeInMilliSec - startTimeInMilliSec;
			diffMinutes = (diffMillSec / (1000 * 60));
		} catch (Exception e) {
			System.out.println("Exception in Utilities timeDifferenceInMinutes():-" + e.getMessage());
		}
		return diffMinutes;
	}

	public static String getTimestamp(long milliSecons) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			long second = (milliSecons / 1000) % 60;
			long minute = (milliSecons / (1000 * 60)) % 60;
			long hour = (milliSecons / (1000 * 60 * 60)) % 24;
			//
			Calendar c = Calendar.getInstance();
			c.setTimeInMillis(milliSecons);
			c.set(Calendar.HOUR_OF_DAY, (int) hour);
			c.set(Calendar.MINUTE, (int) minute);
			c.set(Calendar.SECOND, (int) second);
			return sdf.format(c.getTime());
		} catch (Exception e) {
			System.out.println("Exception in Utilities getTimestamp():-" + e.getMessage());
		}
		return null;
	}

	public static boolean isNumeric(String number) {
		return number.matches("-?\\d+(\\.\\d+)?");
	}

	public static boolean checkIfRowIsEmpty(Row row) {
		if (row == null || row.getLastCellNum() <= 0) {
			return true;
		}
		Cell cell = row.getCell((int) row.getFirstCellNum());
		if (cell == null || "".equals(cell.toString())) {
			return true;
		}
		return false;
	}

	public static long getDiffSecWithCurrentDateFromDbDate(String dbDate) {
		SimpleDateFormat dbSdf;
		Calendar cal_current;
		Calendar cal_future;
		try {
			dbSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

			cal_current = Calendar.getInstance();
			cal_future = Calendar.getInstance();
			cal_future.setTime(dbSdf.parse(dbDate));

			return (cal_future.getTimeInMillis() - cal_current.getTimeInMillis()) / 1000;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return 0;
	}

	public static String getQuizEndDbTime(String startDbTime, int ques_count) throws Exception {

		SimpleDateFormat dbSdf;
		SimpleDateFormat dbSdf_output = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar cal_time;

		int buffer_time_sec = 55;
		int ques_ans_time_sec = 13;
		int total_time_sec = 0;
		try {
			total_time_sec = buffer_time_sec + (ques_ans_time_sec * ques_count);

			if (startDbTime.length() >= 19) {
				dbSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			} else {
				dbSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			}

			cal_time = Calendar.getInstance();
			cal_time.setTime(dbSdf.parse(startDbTime));
			cal_time.add(Calendar.SECOND, total_time_sec);

			return dbSdf_output.format(cal_time.getTime());
		} catch (Exception e) {
			throw e;
		}
	}

	/*
	 * public static void main(String args[]) { System.out.println("============" +
	 * "yyyy-MM-dd HH:mm:ss".length()); }
	 */

	public static String getCurrentDateTime() {
		Calendar cal = Calendar.getInstance();
		TimeZone zone = TimeZone.getTimeZone("IST");
		cal.setTimeZone(zone);
		TimeZone.setDefault(zone);
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		dateFormat.setTimeZone(zone);
		return dateFormat.format(cal.getTime());
	}
}
