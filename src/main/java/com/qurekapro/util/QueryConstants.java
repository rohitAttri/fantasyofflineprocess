package com.qurekapro.util;

public interface QueryConstants {
	
	String UPDATE_USER_LEVEL_FOR_ALL_GAMES="";
	
	String DEACTIVATE_CONTEST_AUTOMATION = "UPDATE `sports_contest_auto_master` SET `is_active`=? WHERE match_id = ?";
	
	String CHECK_CANCELLED_MATCH = "SELECT `status` FROM `sports_matches` WHERE `match_id`=? LIMIT 1";

	String CANCEL_MATCH = "update sports_matches set status = ?, modified = current_timestamp where id=?";
	
	String GET_ALL_ACTIVE_CONTESTS = "SELECT `id` as cid FROM `sports_contests` WHERE match_id = ? and status = ?";

}
