package com.qurekapro.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.qurekapro.service.MatchesService;

@RestController
public class MatchesController {
	
	@Autowired
	MatchesService matchesService;
	
	@GetMapping(value = "/cancel-sports-match")
	public ResponseEntity<Object> processContestRewardUpload() {
		return new ResponseEntity<>(matchesService.processCancelSportsMatch(0),
				HttpStatus.OK);
	}

	@GetMapping(value = "/cancel-sports-match/{requestId}")
	public ResponseEntity<Object> processContestRewardUpload(@PathVariable("requestId") int requestId) {
		return new ResponseEntity<>(matchesService.processCancelSportsMatch(requestId),
				HttpStatus.OK);
	}
}
