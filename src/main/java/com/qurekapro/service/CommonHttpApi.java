package com.qurekapro.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.qurekapro.properties.AppProperties;


@Service
public class CommonHttpApi {

	private static final Logger LOGS = LoggerFactory.getLogger(CommonHttpApi.class);
	@Autowired
	AppProperties appProperties;

	@Autowired
	@Qualifier("restTemplate")
	RestTemplate trackRestTemplate;

	public void callRefundApi(int match_id, int contest_id) throws Exception {
		ResponseEntity<String> response;
		String refundUrl;
		try {
			refundUrl = appProperties.getRefundApiUrl() + match_id + "/" + contest_id;

			LOGS.info("refundUrl :: {}",refundUrl);
			
			response = trackRestTemplate.getForEntity(refundUrl, String.class);

			LOGS.info("Refund api response::" + response);

			LOGS.info("Refund-response status code::" + response.getStatusCodeValue());
			LOGS.info("Refund-response body::" + response.getBody());

		} catch (Exception e) {
			LOGS.error("Exception in callRefundApi: " + e.getMessage());
			throw e;
		}
	}
}
