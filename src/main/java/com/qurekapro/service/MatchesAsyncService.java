package com.qurekapro.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.qurekapro.dao.SportsMatchesDao;
import com.qurekapro.model.SportsMatchCancelRequest;
import com.qurekapro.properties.AppProperties;
import com.qurekapro.util.CommonConstants;

@Service
public class MatchesAsyncService {
	
	private static final Logger log = LoggerFactory.getLogger(MatchesAsyncService.class);
	
	@Autowired
	AppProperties appProperties;
	
	@Autowired
	SportsMatchesDao sportsMatchesDao;
	
	@Autowired
	CommonHttpApi httpApi;
	
	@Async
	public void initializeMatchCancellationProcess(SportsMatchCancelRequest request) {
//		1. check if match is not cancelled else validation error - done
//		2. process request - done
//		3. update status to cancelled and modify time - done
//		4. in active league matches from automation - done
//		5. get all active contest list associated with matches which are not deleted and cancelled
//		6. trigger the match cancellation process with match id and contest id one by one (i will automatically update the status = cancelled(2) of sports_contests).
//		7. Ask for H2h case?
		
		List<Map<String, Object>> cidList = new ArrayList<>();
		
		try {
			sportsMatchesDao.updateSportsMatchCancelRequestId(request.getRequest_id(),CommonConstants.REQUEST_PROCESSING);
			if(!sportsMatchesDao.checkIfMatchIsCancelledAlready(request.getMatch_id())) {
				sportsMatchesDao.deactivateContestAutomation(request.getMatch_id());
				sportsMatchesDao.cancelMatch(request.getMatch_id());
				
				cidList = sportsMatchesDao.getAllActiveContestForMid(request.getMatch_id());
				if(!cidList.isEmpty() && cidList.size()>0) {
					log.info("tigerring refund for cids : {}",cidList.toString());
					for (Map<String, Object> map : cidList) {
						if(map.containsKey("cid")) {
							httpApi.callRefundApi((int)request.getMatch_id(), Integer.valueOf(map.get("cid").toString()));
//							sportsMatchesDao.increaseSportsMatchesCancelRequestRecordStatus(request.getRequest_id(), CommonConstants.SUCCESS);
						}
					}
				}		
//				sportsMatchesDao.increaseSportsMatchesCancelRequestRecordStatus(request.getRequest_id(), CommonConstants.SUCCESS);
//				CommonConstants.FAILURE
//				CommonConstants.VALIDATION_FAIL
			}
			sportsMatchesDao.updateSportsMatchCancelRequestId(request.getRequest_id(),CommonConstants.REQUEST_COMPLETED);
		} catch (Exception e) {
			log.error("Exception in MatchesAsyncService initializeMatchCancellationProcess : {}",e.getMessage());
			e.printStackTrace();
		}
	}
}
