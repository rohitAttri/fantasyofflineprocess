package com.qurekapro.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qurekapro.dao.SportsMatchesDao;
import com.qurekapro.model.SportsMatchCancelRequest;
import com.qurekapro.util.CommonConstants;

@Service
public class MatchesService {
	
	@Autowired
	SportsMatchesDao sportsMatchesDao;

	@Autowired
	MatchesAsyncService matchesAsyncService;

	public Map<String, Object> processCancelSportsMatch(int requestId) {
		Map<String, Object> response = new HashMap<>();
		SportsMatchCancelRequest sportsMatchCancelRequest;
		
		try {
			if (requestId <= 0) {
				response.put("status_msg", "request id zero/invalid");
				response.put("status", 0);
				return response;
			}

			sportsMatchCancelRequest = sportsMatchesDao.getContestRewardUploadRequests(requestId);
			
			if (sportsMatchCancelRequest == null) {
				response.put("status_msg", "request id not found");
				response.put("status", 0);
				return response;
			}

			if (!sportsMatchCancelRequest.getReq_status().equals(CommonConstants.REQUEST_PENDING)) {
				response.put("status_msg", "Already, request id processing");
				response.put("status", 0);
				return response;
			}

			if (sportsMatchCancelRequest.getMatch_id() == 0) {
				response.put("status_msg", "Match id is 0");
				response.put("status", 0);
				return response;
			}	

			matchesAsyncService.initializeMatchCancellationProcess(sportsMatchCancelRequest);

			response.put("status_msg", "success");
			response.put("status", 1);
			return response;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return response;
	}
}
